Visualize time series with [Grafana](http://grafana.org/).

Take a look at the [documentation](http://docs.grafana.org/installation/configuration/)
to learn how you can use environment variables for configuration.

# Volumes
- `/var/lib/grafana/data`

# Ports
- 3000

# Capabilities
- CHOWN
- DAC_OVERRIDE
- FOWNER
- NET_BIND_SERVICE
- SETGID
- SETUID
