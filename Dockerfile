FROM quay.io/thallian/alpine-s6:latest

ENV VERSION 4.1.2-1486989747

RUN addgroup -g 2222 grafana
RUN adduser -h /var/lib/grafana -u 2222 -D -G grafana grafana

RUN apk add --no-cache libressl libc6-compat ca-certificates

RUN ln -s /lib /lib64

RUN wget -qO- https://grafanarel.s3.amazonaws.com/builds/grafana-$VERSION.linux-x64.tar.gz | tar -xz -C /var/lib/grafana --strip 1

ADD /rootfs /

RUN chown -R grafana:grafana /var/lib/grafana

EXPOSE 9090

VOLUME /var/lib/grafana/data
